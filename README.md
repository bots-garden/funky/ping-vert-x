# Ping

Ping is a Vert.x application using this container runtime: `registry.gitlab.com/bots-garden/funky/funky-java-runtime:latest`.

The details of **funky-java-runtime** is here: [https://gitlab.com/bots-garden/funky/funky-java-runtime/container_registry/](https://gitlab.com/bots-garden/funky/funky-java-runtime/container_registry/)

## How to deploy your application

```bash
export KUBECONFIG=path_to_your_kubernetes_config_file
export SUB_DOMAIN="X.X.X.X.nip.io" # or whathever you want
./deploy
```

> - you can override the `RUNTIME_IMAGE` variable
>   - the default value is `registry.gitlab.com/bots-garden/funky/funky-java-runtime:latest`
> - you can override the `NAMESPACE` variable
>   - the default value is `funky-apps-${BRANCH}` where `${BRANCH}` is the current branch

